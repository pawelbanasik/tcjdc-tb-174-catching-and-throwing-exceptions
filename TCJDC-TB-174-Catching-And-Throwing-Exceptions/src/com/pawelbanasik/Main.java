package com.pawelbanasik;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		int result = divide();
		System.out.println(result);

	}

	public static int divide() {
		int x, y;
		try {
			x = getInt();
			y = getInt();
		} catch (NoSuchElementException e) {
			// tak sie nie robi - kod w catch nie moze generowac exceptions
			// w catch robimy maksymalnie malo kodu
			// x = getInt();
			throw new ArithmeticException("no suitable input");
		}
		System.out.println("x is " + x + ", y is " + y); 

		try {
			return x / y;
		} catch (ArithmeticException e) {
			// TODO: handle exception
			throw new ArithmeticException("attempt to divide by zero");
		}

	}

	// dziala tak ze dopoki nie wpiszemy poprawnie danych
	private static int getInt() {
		Scanner s = new Scanner(System.in);
		System.out.println("Please enter an integer ");
		while (true) {
			try {
				return s.nextInt();
			} catch (InputMismatchException e) {
				s.nextLine();
				System.out.println("Please enter a number using only the digits 0 to ");
			}
		}
	}

}
